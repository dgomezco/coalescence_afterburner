//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// G4TheoFSGenerator
//
// 20110307  M. Kelsey -- Add call to new theTransport->SetPrimaryProjectile()
//		to provide access to full initial state (for Bertini)
// 20110805  M. Kelsey -- Follow change to G4V3DNucleus::GetNucleons()

#include "G4DynamicParticle.hh"
#include "G4TheoFSGenerator.hh"
#include "G4ReactionProductVector.hh"
#include "G4ReactionProduct.hh"
#include "G4IonTable.hh"

G4TheoFSGenerator::G4TheoFSGenerator(const G4String& name)
    : G4HadronicInteraction(name), fP0_d(0), fP0_dbar(0)
    , theTransport(0), theHighEnergyGenerator(0)
    , theQuasielastic(0)
 {
 theParticleChange = new G4HadFinalState;
}

G4TheoFSGenerator::~G4TheoFSGenerator()
{
  delete theParticleChange;
}

void G4TheoFSGenerator::ModelDescription(std::ostream& outFile) const
{
  outFile << GetModelName() <<" consists of a " << theHighEnergyGenerator->GetModelName()
		  << " string model and a stage to de-excite the excited nuclear fragment."
		  << "\n<p>"
		  << "The string model simulates the interaction of\n"
          << "an incident hadron with a nucleus, forming \n"
          << "excited strings, decays these strings into hadrons,\n"
          << "and leaves an excited nucleus. \n"
          << "<p>The string model:\n";
  theHighEnergyGenerator->ModelDescription(outFile);
  outFile <<"\n<p>";
  theTransport->PropagateModelDescription(outFile);
}


G4HadFinalState * G4TheoFSGenerator::ApplyYourself(const G4HadProjectile & thePrimary, G4Nucleus &theNucleus)
{
  // init particle change
  theParticleChange->Clear();
  theParticleChange->SetStatusChange(stopAndKill);
  G4double timePrimary=thePrimary.GetGlobalTime();
  //G4cout<<"Model: "<<theHighEnergyGenerator->GetModelName()<<G4endl;
  //coalescence-----------------
  SetP0Coalescence(thePrimary, theHighEnergyGenerator->GetModelName());
  //----------------------------
  
  // check if models have been registered, and use default, in case this is not true @@
  
  const G4DynamicParticle aPart(thePrimary.GetDefinition(),thePrimary.Get4Momentum().vect());

  if ( theQuasielastic ) {
  
     if ( theQuasielastic->GetFraction(theNucleus, aPart) > G4UniformRand() )
     {
       //G4cout<<"___G4TheoFSGenerator: before Scatter (1) QE=" << theQuasielastic<<G4endl;
       G4KineticTrackVector *result= theQuasielastic->Scatter(theNucleus, aPart);
       //G4cout << "^^G4TheoFSGenerator: after Scatter (1) " << G4endl;
       if (result)
       {
	    for(unsigned int  i=0; i<result->size(); i++)
	    {
	      G4DynamicParticle * aNew = 
		 new G4DynamicParticle(result->operator[](i)->GetDefinition(),
                        	       result->operator[](i)->Get4Momentum().e(),
                        	       result->operator[](i)->Get4Momentum().vect());
	      theParticleChange->AddSecondary(aNew);
	      delete result->operator[](i);
	    }
	    delete result;
	   
       } else 
       {
	    theParticleChange->SetStatusChange(isAlive);
	    theParticleChange->SetEnergyChange(thePrimary.GetKineticEnergy());
	    theParticleChange->SetMomentumChange(thePrimary.Get4Momentum().vect().unit());
 
       }
	return theParticleChange;
     } 
  }

 // get result from high energy model

  G4KineticTrackVector * theInitialResult =
               theHighEnergyGenerator->Scatter(theNucleus, aPart);

//#define DEBUG_initial_result
  #ifdef DEBUG_initial_result
  	  G4double E_out(0);
  	  G4IonTable * ionTable=G4ParticleTable::GetParticleTable()->GetIonTable();
  	  std::vector<G4KineticTrack *>::iterator ir_iter;
  	  for(ir_iter=theInitialResult->begin(); ir_iter!=theInitialResult->end(); ir_iter++)
  	  {
  		  //G4cout << "TheoFS secondary, mom " << (*ir_iter)->GetDefinition()->GetParticleName() << " " << (*ir_iter)->Get4Momentum() << G4endl;
  		  E_out += (*ir_iter)->Get4Momentum().e();
  	  }
  	  G4double init_mass= ionTable->GetIonMass(theNucleus.GetZ_asInt(),theNucleus.GetA_asInt());
          G4double init_E=aPart.Get4Momentum().e();
  	  // residual nucleus

  	  const std::vector<G4Nucleon> & thy = theHighEnergyGenerator->GetWoundedNucleus()->GetNucleons();

  	  G4int resZ(0),resA(0);
	  G4double delta_m(0);
  	  for(size_t them=0; them<thy.size(); them++)
  	  {
   	     if(thy[them].AreYouHit()) {
  	       ++resA;
  	       if ( thy[them].GetDefinition() == G4Proton::Proton() ) {
	          ++resZ;
		  delta_m +=G4Proton::Proton()->GetPDGMass();
	       } else {
	          delta_m +=G4Neutron::Neutron()->GetPDGMass();
	       }  
  	     }
	  }

  	  G4double final_mass(0);
	  if ( theNucleus.GetA_asInt() ) {
	   final_mass=ionTable->GetIonMass(theNucleus.GetZ_asInt()-resZ,theNucleus.GetA_asInt()- resA);
  	  }
	  G4double E_excit=init_mass + init_E - final_mass - E_out;
	  G4cout << " Corrected delta mass " << init_mass - final_mass - delta_m << G4endl;
  	  G4cout << "initial E, mass = " << init_E << ", " << init_mass << G4endl;
  	  G4cout << "  final E, mass = " << E_out <<", " << final_mass << "  excitation_E " << E_excit << G4endl;
  #endif

  G4ReactionProductVector * theTransportResult = NULL;
  
// Uzhi Nov. 2012
  G4V3DNucleus* theProjectileNucleus = theHighEnergyGenerator->GetProjectileNucleus(); 
if(theProjectileNucleus == 0)                                       // Uzhi Nov. 2012
{                                                                   // Uzhi Nov. 2012

  G4int hitCount = 0;
  const std::vector<G4Nucleon>& they = theHighEnergyGenerator->GetWoundedNucleus()->GetNucleons();
  for(size_t them=0; them<they.size(); them++)
  {
    if(they[them].AreYouHit()) hitCount ++;
  }
  if(hitCount != theHighEnergyGenerator->GetWoundedNucleus()->GetMassNumber() )
  {
    theTransport->SetPrimaryProjectile(thePrimary);	// For Bertini Cascade
    theTransportResult = 
               theTransport->Propagate(theInitialResult, theHighEnergyGenerator->GetWoundedNucleus());
    if ( !theTransportResult ) {
       G4cout << "G4TheoFSGenerator: null ptr from transport propagate " << G4endl;
       throw G4HadronicException(__FILE__, __LINE__, "Null ptr from transport propagate");
    } 
  }
  else
  {
    theTransportResult = theDecay.Propagate(theInitialResult, theHighEnergyGenerator->GetWoundedNucleus());
    if ( !theTransportResult ) {
       G4cout << "G4TheoFSGenerator: null ptr from decay propagate " << G4endl;
       throw G4HadronicException(__FILE__, __LINE__, "Null ptr from decay propagate");
    }   
  }

} else                                                              // Uzhi Nov. 2012
{                                                                   // Uzhi Nov. 2012
    theTransport->SetPrimaryProjectile(thePrimary);
    theTransportResult = 
    theTransport->PropagateNuclNucl(theInitialResult, 
                            theHighEnergyGenerator->GetWoundedNucleus(),
                            theHighEnergyGenerator->GetProjectileNucleus());
    if ( !theTransportResult ) {
       G4cout << "G4TheoFSGenerator: null ptr from transport propagate " << G4endl;
       throw G4HadronicException(__FILE__, __LINE__, "Null ptr from transport propagate");
    } 
}           
                                                       // Uzhi Nov. 2012
  //coalescence-----------------
  //Generate antideuterons using coalescence
  GenerateDeuterons(theTransportResult);
  //----------------------------                                                       
                                                       
  // Fill particle change
  for(auto i=theTransportResult->begin(); i!=theTransportResult->end(); i++)
  {
    G4DynamicParticle * aNewDP =
       new G4DynamicParticle((*i)->GetDefinition(),
                             (*i)->GetTotalEnergy(),
                             (*i)->GetMomentum());

	G4HadSecondary aNew = G4HadSecondary(aNewDP);
    G4double time=(*i)->GetFormationTime();
    if(time < 0.0) { time = 0.0; }
    aNew.SetTime(timePrimary + time);
    aNew.SetCreatorModelType((*i)->GetCreatorModel());
    theParticleChange->AddSecondary(aNew);
    delete (*i);
  }
  
  // some garbage collection
  delete theTransportResult;

  // Done
  return theParticleChange;
}

std::pair<G4double, G4double> G4TheoFSGenerator::GetEnergyMomentumCheckLevels() const
{
  if ( theHighEnergyGenerator ) {
	 return theHighEnergyGenerator->GetEnergyMomentumCheckLevels();
  } else {
	 return std::pair<G4double, G4double>(DBL_MAX, DBL_MAX);
  }
}

//coalescence-----------------

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void G4TheoFSGenerator::GenerateDeuterons(G4ReactionProductVector * result) const
{
//
// Clusters are made with the first nucleon pair that fulfill
// the coalescence conditions, starting with the protons
//
// a deuteron is a pair (i,j) where i is the proton and j the neutron in current event
// with the relative momentum less than p0 (within a sphere of radius p0)
//
//

	std::vector<std::pair<G4int, G4ThreeVector>> proton;
	std::vector<std::pair<G4int, G4ThreeVector>> neutron;
	std::vector<std::pair<G4int, G4ThreeVector>> antiproton;
	std::vector<std::pair<G4int, G4ThreeVector>> antineutron;


	//classifying nucleons from reaction vector
  	for(unsigned int i=0; i<result->size(); i++)
  	{
		G4int pdgid = result->operator[](i)->GetDefinition()->GetPDGEncoding();

		if (pdgid == 2212){
			proton.push_back(std::make_pair(i, result->operator[](i)->GetMomentum()));
			result->erase(result->begin()+i);
		}
  	}

  	for(unsigned int i=0; i<result->size(); i++)
  	{
		G4int pdgid = result->operator[](i)->GetDefinition()->GetPDGEncoding();
		if (pdgid == 2112){
			neutron.push_back(std::make_pair(i, result->operator[](i)->GetMomentum()));
			result->erase(result->begin()+i);
		}
  	}


  	for(unsigned int i=0; i<result->size(); i++)
  	{
		G4int pdgid = result->operator[](i)->GetDefinition()->GetPDGEncoding();

		if (pdgid == -2212){
			antiproton.push_back(std::make_pair(i, result->operator[](i)->GetMomentum()));
			//cout<<"antiproton rejected: "<<i<<"	"<<result->operator[](i)->GetTotalEnergy()<<"		"<<result->operator[](i)->GetMomentum().mag()<<G4endl;
			result->erase(result->begin()+i);
		}
	}

  	for(unsigned int i=0; i<result->size(); i++)
  	{
		G4int pdgid = result->operator[](i)->GetDefinition()->GetPDGEncoding();

		if (pdgid == -2112){
			antineutron.push_back(std::make_pair(i, result->operator[](i)->GetMomentum()));
			//cout<<"antineutron rejected: "<<i<<"	"<<result->operator[](i)->GetTotalEnergy()<<"		"<<result->operator[](i)->GetMomentum().mag()<<G4endl;
			result->erase(result->begin()+i);
		}
	}


	//checking coalescence conditions

	for(unsigned int i=0; i<proton.size(); ++i) // loop over protons 
	{
		if(proton.at(i).first==-1) continue;  // with next proton
		
		G4ThreeVector p1 = proton.at(i).second;

		int partner1 = this->FindPartner(p1, G4Proton::Proton()->GetPDGMass(), neutron, G4Neutron::Neutron()->GetPDGMass(), 1);
		
		if(partner1 == -1) { // if proton discarded, then put it in final vector. 
			
			G4ParticleDefinition* prt = G4ParticleTable::GetParticleTable()->FindParticle("proton");		
			G4ReactionProduct * finalp = new G4ReactionProduct();
			finalp->SetDefinition(prt);
			G4double massp = prt->GetPDGMass();
			G4double E = std::sqrt(p1.mag()*p1.mag()+massp*massp);	
			finalp->SetMomentum(p1);
			finalp->SetTotalEnergy(E);
			finalp->SetMass(massp);	
			result->push_back(finalp);		
			continue; // with next proton
		}
		
		G4ThreeVector p2 = neutron.at(partner1).second;

		this->PushDeuteron(p1, p2, result, 1);

		// tag the bound neutron
		neutron.at(partner1).first = -1;
	}

	for(unsigned int i=0; i<neutron.size(); ++i) // injecting unused neutrons 
	{
		if(neutron.at(i).first==-1) continue;  // with next neutron

		G4ParticleDefinition* nrt = G4ParticleTable::GetParticleTable()->FindParticle("neutron");		
		G4ReactionProduct * finaln = new G4ReactionProduct();//theTransportResult->operator[](i);
		finaln->SetDefinition(nrt);
		
		G4ThreeVector p2 = neutron.at(i).second;
		G4double massn = nrt->GetPDGMass();
		G4double E = std::sqrt(p2.mag()*p2.mag()+massn*massn);	
		finaln->SetMomentum(p2);
		finaln->SetTotalEnergy(E);
		finaln->SetMass(massn);	
		result->push_back(finaln);			
	}



	for(unsigned int i=0; i<antiproton.size(); ++i) // loop over antiprotons
	{
		if(antiproton.at(i).first==-1) continue;  // with next antiproton
		
		G4ThreeVector p1 = antiproton.at(i).second;

		int partner1 = this->FindPartner(p1, G4Proton::Proton()->GetPDGMass(), antineutron, G4Neutron::Neutron()->GetPDGMass(), -1);
		
		if(partner1 == -1) {
			
			G4ParticleDefinition* pbar = G4ParticleTable::GetParticleTable()->FindAntiParticle("proton");		
			G4ReactionProduct * finalpbar = new G4ReactionProduct();
			finalpbar->SetDefinition(pbar);

			G4double massp = pbar->GetPDGMass();
			G4double E = std::sqrt(p1.mag()*p1.mag()+massp*massp);	
			//cout<<"antiproton injected: "<<i<<"	"<<E<<"		"<<p1.mag()<<G4endl;
			finalpbar->SetMomentum(p1);
			finalpbar->SetTotalEnergy(E);
			finalpbar->SetMass(massp);	
			result->push_back(finalpbar);		
			continue; // with next antiproton
		}
		
		G4ThreeVector p2 = antineutron.at(partner1).second;

		this->PushDeuteron(p1, p2, result, -1);
		
		// tag the bound antineutron
		antineutron.at(partner1).first = -1;
	}

	for(unsigned int i=0; i<antineutron.size(); ++i) // injecting unused antineutrons 
	{
		if(antineutron.at(i).first==-1) continue;  // with next antineutron

		G4ParticleDefinition* nbar = G4ParticleTable::GetParticleTable()->FindAntiParticle("neutron");		
		G4ReactionProduct * finalnbar = new G4ReactionProduct();
		finalnbar->SetDefinition(nbar);
		G4ThreeVector p2 = antineutron.at(i).second;
		G4double massn = nbar->GetPDGMass();
		G4double E = std::sqrt(p2.mag()*p2.mag()+massn*massn);	
		//cout<<"antineutron injected: "<<i<<"	"<<E<<"		"<<p2.mag()<<G4endl;
		finalnbar->SetMomentum(p2);
		finalnbar->SetTotalEnergy(E);
		finalnbar->SetMass(massn);	
		result->push_back(finalnbar);			
	}

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4int G4TheoFSGenerator::FindPartner(const G4ThreeVector& p1, G4double m1, std::vector<std::pair<G4int, G4ThreeVector>> & Neutron, G4double m2, G4int charge) const
{
//
// find a nucleon partner within a sphere of radius p0 center at the proton
// and exclude nucleon nx
//

		for(unsigned int j=0; j<Neutron.size(); ++j)
		{
			if(Neutron.at(j).first == -1) continue;  // with next nucleon
		
			G4ThreeVector p2 = Neutron.at(j).second;
		
			if(!this->Coalescence(p1,m1,p2,m2,charge)) continue; // with next nucleon
		
			return j;
		}

		return -1; // no partner
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4bool G4TheoFSGenerator::Coalescence( G4double p1x, G4double p1y, G4double p1z, G4double m1
                                 , G4double p2x, G4double p2y, G4double p2z, G4double mass2, G4int charge) const
{
//
// returns true if the nucleons are inside of an sphere of radius p0
// (assume the nucleons are in the same place)
//

	G4double deltaP = this->GetPcm( p1x, p1y, p1z, m1, p2x, p2y, p2z, mass2);
	//G4cout<<fP0<<G4endl;
	if(charge>0) return (deltaP < fP0_d);
		else return (deltaP < fP0_dbar);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4bool G4TheoFSGenerator::Coalescence(const G4ThreeVector& p1, G4double m1, const G4ThreeVector& p2, G4double mass2, G4int charge) const
{
//
// returns true if the nucleons are inside of an sphere of radius p0
// (assume the nucleons are in the same place)
//
	return this->Coalescence(  p1.x(), p1.y(), p1.z(), m1
	                         , p2.x(), p2.y(), p2.z(), mass2, charge);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void G4TheoFSGenerator::PushDeuteron(const G4ThreeVector& p1, const G4ThreeVector& p2, G4ReactionProductVector * result, G4int charge) const
{

	if(charge > 0) {
		G4ParticleDefinition* deuteron = G4ParticleTable::GetParticleTable()->FindParticle("deuteron");		
		G4ReactionProduct * finaldeut = new G4ReactionProduct();
		finaldeut->SetDefinition(deuteron);

		G4ThreeVector pT = p1+p2;
		G4double massd = deuteron->GetPDGMass();
		G4double E = std::sqrt(pT.mag()*pT.mag()+massd*massd);	
		finaldeut->SetMomentum(pT);
		finaldeut->SetTotalEnergy(E);
		finaldeut->SetMass(massd);
		result->push_back(finaldeut);

	} else {

		G4ParticleDefinition* antideuteron = G4ParticleTable::GetParticleTable()->FindAntiParticle("deuteron");	
		G4ReactionProduct * finalantideut = new G4ReactionProduct();
		finalantideut->SetDefinition(antideuteron);

		G4ThreeVector pT = p1+p2;
		G4double massd = antideuteron->GetPDGMass();
		G4double E = std::sqrt(pT.mag()*pT.mag()+massd*massd);	
		finalantideut->SetMomentum(pT);
		finalantideut->SetTotalEnergy(E);
		finalantideut->SetMass(massd);
		result->push_back(finalantideut);
	}

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void G4TheoFSGenerator::SetP0Coalescence(const G4HadProjectile & thePrimary, G4String Model)
{
	//Colaescence condition just available for proton-A, antiproton-A collisions.

	//G4cout<<"Projectile: "<<thePrimary.GetDefinition()->GetParticleName()<<G4endl;
	//if(Model=="FTF"){

		if(std::abs(thePrimary.GetDefinition()->GetPDGEncoding())==2212) {

			//G4int n = target->GetN();
			//G4String targetName = target->GetName();
			G4double mproj = thePrimary.GetDefinition()->GetPDGMass();
			G4double pz = thePrimary.Get4Momentum().z();
			G4double ekin = std::sqrt(pz*pz+mproj*mproj)-mproj;	
	
			if(thePrimary.GetDefinition()->GetPDGEncoding()==2212){

				if(ekin>10.0){
					fP0_dbar = 130./(1+std::exp(21.6-std::log(0.001*ekin)/0.089));//set p0 antideuteron
				} else fP0_dbar = 0.0;

				if(ekin>10.0){
					fP0_d = 118.1*(1+std::exp(5.53-std::log(0.001*ekin)/0.43)); //set p0 deuteron
				} else fP0_d = 0.0;
				
			} 
			else { fP0_dbar = 0.0;
					fP0_d = 0.0;
				//if(rs>4.){
            	//	fP0 = 188.5/(1+std::exp(6.09-std::log(0.001*rs+2.)/0.48));
				//} else fP0 = 0.0;
			}

			if(fP0_dbar<0.0) fP0_dbar = 0.0;
			if(fP0_d<0.0) fP0_d = 0.0;

		} else fP0_dbar = 0.0, fP0_d = 0.0;

	//} else fP0_dbar = 80.0, fP0_d = 80.0;

	G4cout<<"Coalescence parameter p0 deuteron / antideuteron: "<<fP0_d<<"	/  "<<fP0_dbar<<G4endl;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4double G4TheoFSGenerator::GetS(G4double p1x, G4double p1y, G4double p1z, G4double m1, G4double p2x, G4double p2y, G4double p2z, G4double mass2) const
{
//
// square of center of mass energy of two particles from LAB values
//
	G4double E1 = std::sqrt( p1x*p1x + p1y*p1y + p1z*p1z + m1*m1);
	G4double E2 = std::sqrt( p2x*p2x + p2y*p2y + p2z*p2z + mass2*mass2);	

	return (E1+E2)*(E1+E2) - ((p1x+p2x)*(p1x+p2x) + (p1y+p2y)*(p1y+p2y) + (p1z+p2z)*(p1z+p2z));
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4double G4TheoFSGenerator::GetPcm(G4double p1x, G4double p1y, G4double p1z, G4double m1, G4double p2x, G4double p2y, G4double p2z, G4double mass2) const
{
//
// momentum in the CM frame of two particles from LAB values
//
	G4double scm = this->GetS(p1x, p1y, p1z, m1, p2x, p2y, p2z, mass2);
	
	return std::sqrt( (scm-(m1-mass2)*(m1-mass2))*(scm-(m1+mass2)*(m1+mass2)) )/(2.*std::sqrt(scm));
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4double G4TheoFSGenerator::GetPcm(const G4ThreeVector& p1, G4double m1, const G4ThreeVector& p2, G4double mass2) const
{
//
// momentum in the CM frame of two particles from LAB values
//
	return this->GetPcm(p1.x(),p1.y(),p1.z(),m1,p2.x(),p2.y(),p2.z(),mass2);
}
