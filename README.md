Geant4 coalescence afterburner

The code is a modified version of the original class G4TheoFSGenerator located in:
 > geant4_source_code/source/processes/hadronic/models/theo_high_energy/

Modifications and additions allow to produce deuterons and antideuterons using the coalescence model,
without altering the final particle vector obtainde from other geant4 processes. The model states that 
nucleons can merger into a complex nuclei by being close in phase space. The free parameter that describes 
the probability this happens is the coalescence momentum (p_0).

//--------------------------------------------

Installation

1. Replace files G4TheoFSGenerator.cc and G4TheoFSGenerator.hh by the originals in:

 > your-geant4-source-code/source/processes/hadronic/models/theo_high_energy/src

and

 > your-geant4-source-code/source/processes/hadronic/models/theo_high_energy/include 

respectively.

2. Then re-compile your geant4 code: 

 > ~build_dir$ make
 > ~build_dir$ make install

3. execute example as usual, in the output should be appear the next text:

Coalescence parameter p0 deuteron / antideuteron: # / #


//--------------------------------------------

Tested in geant4.10.06

//______________________

Acknowledgments

This work was supported by UNAM-PAPIIT IA101624 and CONAHCYT CF-2019/2042.

 

 
